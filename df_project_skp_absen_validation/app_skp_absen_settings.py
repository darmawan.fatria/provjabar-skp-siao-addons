from openerp.osv import fields, osv
from datetime import datetime, date
from mx import DateTime

class app_skp_absen_settings(osv.osv_memory):
    _name = 'app.skp.absen.settings'
    _description = 'Konfigurasi Aplikasi SKP Dan absen'

    _columns = {
            'name' : fields.char('Konfigurasi Integrasi SKP absen', size=64, readonly=True),
            'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
            'target_period_year'     : fields.char('Periode Tahun', size=4, ),
    }
    #real automitation process [2016-02-09]
    def action_auto_validate_to_project_perilaku_format_scheduler(self, cr, uid, exec_date,context=None):

        data_input_pool = self.pool.get('project.perilaku.absen.data.integration')
        now=DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        target_period_year=last_month.strftime('%Y')
        target_period_month=last_month.strftime('%m')
        date_now=now.strftime('%d')
        print "year : ",target_period_year
        print "month :",target_period_month
        print "exec date v  date now : ",exec_date, " v ", date_now
        if exec_date :
            data_ids        = data_input_pool.search(cr, uid, [
                                                   ('periode_tahun', '=', target_period_year),
                                                   ('periode_bulan', '=', target_period_month),
                                                   ('state','=','new')
                                                   ],
                                        context=None,limit=1000)
            if data_ids :
                    print "data yang akan sinkron : ",len(data_ids)
                    data_input_pool.extract_to_project_perilaku(cr,uid,data_ids,context=None)
        
        return {}

    def action_auto_replace_data_project_perilaku_with_absen_scheduler(self, cr, uid, exec_date,context=None):

        data_extraction_pool = self.pool.get('project.perilaku.absen.data.extraction')
        now=DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        target_period_year=last_month.strftime('%Y')
        target_period_month=last_month.strftime('%m')
        date_now=now.strftime('%d')
        print "year : ",target_period_year
        print "month :",target_period_month
        print "exec date v  date now : ",exec_date, " v ", date_now
        if exec_date :
            data_ids        = data_extraction_pool.search(cr, uid, [
                                                   ('target_period_year', '=', target_period_year),
                                                   ('target_period_month', '=', target_period_month),
                                                   ('state','=','new')
                                                   ],
                                        context=None)
            if data_ids :
                    print "data yang akan sinkron : ",len(data_ids)
                    data_extraction_pool.sync_and_update_with_project_perilaku(cr,uid,data_ids,context=None)

        return {}


    #end real automation
    def action_validate_to_project_perilaku_format_scheduler(self, cr, uid, context=None):

        data_input_pool = self.pool.get('project.perilaku.absen.data.integration')
        data_ids        = data_input_pool.search(cr, uid, [
                                               ('periode_tahun', '=', '2015'), 
                                               ('periode_bulan', '=', '05'),
                                               ('state','=','new')
                                               ], 
                                    context=None,limit=1000)
        if data_ids:
                print "data yang akan sinkron : ",len(data_ids)
                data_input_pool.extract_to_project_perilaku(cr,uid,data_ids,context=None)
        
        return {}
    def action_validate_to_project_perilaku_format(self, cr, uid, ids, context={}):

        data_input_pool = self.pool.get('project.perilaku.absen.data.integration')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids        = data_input_pool.search(cr, uid, [('state', '=', 'new'),
                                               ('periode_tahun', '=', param_obj.target_period_year), 
                                               ('periode_bulan', '=', param_obj.target_period_month)], 
                                    context=None,limit=100)
                if data_ids:
                    print "data yang akan sinkron : ",len(data_ids)
                    data_input_pool.extract_to_project_perilaku(cr,uid,data_ids,context=None)
        
        return {}
    def action_replace_data_project_perilaku_with_absen(self, cr, uid, ids, context={}):
        
        data_extraction_pool = self.pool.get('project.perilaku.absen.data.extraction')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids        = data_extraction_pool.search(cr, uid, [('state', '=', 'new'),
                                               ('target_period_year', '=', param_obj.target_period_year), 
                                               ('target_period_month', '=', param_obj.target_period_month)], 
                                    context=None,limit=700)
                if data_ids:
                    print "data yang akan sinkron : ",len(data_ids)
                    data_extraction_pool.sync_and_update_with_project_perilaku(cr,uid,data_ids,context=None)
        return True
    def action_rollback_project_skp_with_absen(self, cr, uid, ids, context={}):
        
        data_extraction_pool = self.pool.get('project.perilaku.absen.data.extraction')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids        = data_extraction_pool.search(cr, uid, [('state', '=', 'synced'),
                                               ('target_period_year', '=', param_obj.target_period_year), 
                                               ('target_period_month', '=', param_obj.target_period_month)], 
                                    context=None)
                if data_ids:
                    print "data yang akan dirollback sinkron : ",len(data_ids)
                data_extraction_pool.rollback_and_set_evaluated_sync_with_project_perilaku(cr,uid,data_ids,context=None)
        return True
    
app_skp_absen_settings()
