from openerp.osv import fields, osv
from mx import DateTime
import time



class project_perilaku(osv.Model):
    _inherit = 'project.perilaku'

    def unlink(self, cr, uid, ids, context=None):
        return self.set_cancelled(cr,uid,ids,context=None)
    
    _columns = {
        'absen_sync_date'     : fields.date('Tanggal Sinkronisasi Dengan Absen',readonly=True),
        'is_absen_sync'       : fields.boolean('Sinkronisasi Absen'),
        'absen_sync_state'       : fields.selection([     ('not_synced', 'Tidak Ada Data Sinkronisasi'), 
                                                         ('synced', 'Synced'),
                                  ],'Status Sinkronisasi Absen',),
        'old_realisasi_hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi'),
        'old_realisasi_hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja'),
        'old_realisasi_hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja'),
        'old_realisasi_hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar'),
        'data_extraction_id': fields.many2one('project.perilaku.absen.data.extraction',"Detail Komitmen Dan Disiplin" ),
    }
   
    def manual_syncronize_skp_siao_integration(self, cr, uid, ids,context=None):
        data_extraction_pool = self.pool.get('project.perilaku.absen.data.extraction')
        for task_id in ids:
            data_extraction_pool.sync_and_update_with_project_skp_by_task_id(cr,uid,task_id,context=None)
        return True
    def manual_rollback_absen_integration(self, cr, uid, ids,context=None):
        for task_obj in self.browse(cr,uid,ids,context=None):
            task =  {}
            task.update({
                                    'old_realisasi_hadir_hari_kerja':0,
                                    'old_realisasi_hadir_jam_kerja':0,
                                    'old_realisasi_hadir_apel_pagi':0,
                                    'old_realisasi_hadir_upacara_hari_besar':0,
                                    'realisasi_hadir_hari_kerja':task_obj.old_realisasi_hadir_hari_kerja,
                                    'realisasi_hadir_jam_kerja':task_obj.realisasi_hadir_jam_kerja,
                                    'realisasi_hadir_apel_pagi':task_obj.realisasi_hadir_apel_pagi,
                                    'realisasi_hadir_upacara_hari_besar':task_obj.realisasi_hadir_upacara_hari_besar,
                                    'absen_sync_date':None,
                                    'is_absen_sync':False,
                                    'absen_sync_state':'not_synced',
                        })
            self.write(cr,uid,task_obj.id,task,context=None) 
        
        return True  
    def action_done(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        #get var lock action done ?
        is_lock=False;
        try :
            lock_absen_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'lck_abs'), ('active', '=', True), ('type', '=', 'lain_lain')], context=None)
            is_lock_int  = self.pool.get('config.skp').browse(cr, uid, lock_absen_ids)[0].config_value_int
            if is_lock_int == 1:
                is_lock=True;
            else :
                is_lock=False;
        except :
            is_lock=False;

        if is_lock :
            #print "Filter only done perilaku...."
            duplicate_ids = self.read(cr, uid, ids, ['id','is_absen_sync','absen_sync_state',], context=context)
            task_ids=[]
            for record in duplicate_ids:
                if record['is_absen_sync'] :
                    task_ids.append(record['id'])
                else :
                    continue;
            super(project_perilaku, self).action_done(cr, uid, task_ids, context=context)
        else :
            super(project_perilaku, self).action_done(cr, uid, ids, context=context)
        
        return True
    
      
project_perilaku()

class hari_kerja_bulanan(osv.Model):
    _inherit = "hari.kerja.bulanan"
    
   
    _columns = {
        'name'             : fields.char('Periode',size=6),  
        'jadwal_apel_pagi_ids'     : fields.one2many('jadwal.apel.pagi','hari_kerja_bulanan_id', 'Tanggal Apel Pagi', required=True,),
        'jadwal_uhbn_ids'          : fields.one2many( 'jadwal.uhbn','hari_kerja_bulanan_id','Tanggal Upacara Hari Besar Nasional', required=True,), 
    }
    _defaults = {
    'name':lambda *args: time.strftime('%Y%m'),
   }
hari_kerja_bulanan()

class jadwal_apel_pagi(osv.Model):
    _name = "jadwal.apel.pagi"
    _description    ="Jadwal Apel Pagi - SIAO"
   
    _columns = {
        'name'             : fields.char('Data',size=50),  
        'tanggal_apel'              :fields.date('Tanggal Apel',required=True),
        'hari_kerja_bulanan_id': fields.many2one('hari.kerja.bulanan', 'Data Singkronisasi',),
    }
    _defaults ={
                'active' : 'True',
                }
jadwal_apel_pagi()
class jadwal_uhbn(osv.Model):
    _name = "jadwal.uhbn"
    _description    ="Jadwal Upacara Hari Besar - SIAO"
   
    _columns = {
        'name'             : fields.char('Data',size=50),  
        'tanggal_upacara'              :fields.date('Tanggal Upacara',required=True),
        'hari_kerja_bulanan_id': fields.many2one('hari.kerja.bulanan', 'OPD',),
    }
    _defaults ={
                'active' : 'True',
                }
jadwal_uhbn()
