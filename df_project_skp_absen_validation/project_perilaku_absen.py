from openerp.osv import fields, osv
import time
from mx import DateTime

class project_perilaku_absen_data_integration(osv.Model):
    _name = "project.perilaku.absen.data.integration"
    _description='Integrasi Data SKP - absen'
  
    _columns = {
        'periode_tahun' :fields.char('Periode Tahun',size=4,required=False),
        'periode_bulan' :fields.char('Periode Bulan',size=2,required=False),
        'target_period_year' :fields.char('Periode Tahun',size=4,required=False),
        'target_period_month' :fields.char('Periode Bulan',size=2,required=False),
        'nip' :fields.char('NIP',size=18,required=True),
        
        'jumlah_masuk_kerja' :fields.integer('Jumlah Masuk Kerja',size=2, readonly=False),
        'jumlah_dinas_luar' :fields.integer('Jumlah Dinas Luar',size=2, readonly=False),
        'jumlah_diklat' :fields.integer('Jumlah Diklat',size=2, readonly=False),
        'jumlah_jam_kerja_normal' :fields.integer('Jumlah Jam Kerja Normal',size=3, readonly=False),
        'jumlah_jam_kerja_tambahan' :fields.integer('Jumlah Jam Kerja Tambahan',size=3, readonly=False),
        'jumlah_hadir_apel_pagi' :fields.integer('Hadir Apel Pagi',size=1, readonly=False),
        'jumlah_hadir_upacara_hari_besar' :fields.integer('Hadir Upacara Hari Besar',size=1, readonly=False),
        
        'state': fields.selection([     ('new', 'Baru'), 
                                        ('skp', 'SKP'), 
                                        ('synced', 'Synced'),
                                        ('failed', 'Failed'),
                                        ('done', 'Done'),
                                                      ],
                                        'Status',
                                                     ),
        'notes': fields.text('Catatan' ),
        'tgl_sinkron_absen' :fields.date('Tanggal Sinkornisasi absen',),
        'tgl_sinkron_skp' :fields.date('Tanggal Sinkornisasi SKP',),
        
    }
    _defaults = {
        'tgl_sinkron_absen':lambda *args: time.strftime('%Y-%m-%d'),
 #       'periode_tahun':lambda *args: time.strftime('%Y'),
#        'periode_bulan':lambda *args: time.strftime('%m'),
        'state':'new',
        
        
    }
    def extract_to_project_perilaku(self, cr, uid, ids,context=None):
        data_extraction_pool = self.pool.get('project.perilaku.absen.data.extraction')
        project_perilaku_pool = self.pool.get('project.perilaku')
        sync_ids = []
        no_task_failed_sync_ids = []
        duplicate_task_failed_sync_ids = []
        for data_integration in self.browse(cr, uid, ids, context=context):
            code_result_messages=''
            data_extraction =  {}
            
            domain = data_integration.nip,data_integration.periode_tahun,data_integration.periode_bulan,
            cr.execute("""select p.id,p.name,p.target_period_month,p.target_period_year,
                                p.company_id,p.user_id
                       from project_perilaku p
                       left join res_partner emp on p.user_id = emp.user_id
                       where
                            emp.nip = %s
                            and p.target_period_year = %s
                            and p.target_period_month = %s
                       and p.active
                  
                        """ ,domain)
                
            result = cr.fetchall()
            
            if not result:
                code_result_messages='Tidak Ada Realisasi Perilaku Di Periode Bulan Ini'
                no_task_failed_sync_ids.append(data_integration.id)
            else:
                code_result_messages='Sukses'
                for perilaku_id, name, target_period_month,target_period_year, company_id, user_id in result:
                    jumlah_hari_kerja = data_integration.jumlah_masuk_kerja+data_integration.jumlah_dinas_luar+data_integration.jumlah_diklat
                    jumlah_jam_kerja = data_integration.jumlah_jam_kerja_normal + data_integration.jumlah_jam_kerja_tambahan
                    data_extraction.update({
                                'name':name,
                                'target_period_month':target_period_month,
                                'target_period_year':target_period_year,
                                'project_perilaku_id':perilaku_id,
                                'company_id':company_id,
                                'user_id':user_id,
                                'notes':code_result_messages,
                                'jumlah_masuk_kerja':data_integration.jumlah_masuk_kerja,
                                'jumlah_dinas_luar':data_integration.jumlah_dinas_luar,
                                'jumlah_diklat':data_integration.jumlah_diklat,
                                'jumlah_jam_kerja_normal':data_integration.jumlah_jam_kerja_normal,
                                'jumlah_jam_kerja_tambahan':data_integration.jumlah_jam_kerja_tambahan,
                                'jumlah_hari_kerja':jumlah_hari_kerja,
                                'jumlah_jam_kerja':jumlah_jam_kerja,
                                'jumlah_hadir_apel_pagi':data_integration.jumlah_hadir_apel_pagi,
                                'jumlah_hadir_upacara_hari_besar':data_integration.jumlah_hadir_upacara_hari_besar,
                                'project_skp_absen_id':data_integration.id,
                                'state':'new'
                                })
                    extract_id = data_extraction_pool.create(cr,uid,data_extraction,context=None)
                    sync_ids.append(data_integration.id)
            #print "code_result_messages : ",code_result_messages
        now=time.strftime('%Y-%m-%d')
        #sync_success
        if sync_ids:
            self.write(cr, uid, sync_ids, {'state':'synced',
                                      'notes':'success',
                                      'tgl_sinkron_skp':now,
                                      }, context=None) 
        #failed
        if no_task_failed_sync_ids:
            self.write(cr, uid, no_task_failed_sync_ids, {'state':'failed',
                                  'notes':'Tidak Ada Kode Kegiatan Di Periode Bulan Ini',
                                  }, context=None)
        
        return True
project_perilaku_absen_data_integration()

class project_perilaku_absen_data_extraction(osv.Model):
    _name = 'project.perilaku.absen.data.extraction'
    _description='Extract Data SKP - absen' 
    _columns = {
        'name': fields.char('Nama', size=100, ),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     , readonly=True),
        'target_period_year'     : fields.char('Periode Tahun', size=4, readonly=True),
        'project_perilaku_id': fields.many2one('project.perilaku', 'Realisasi Perilaku', required=True, readonly=True),
        'project_skp_absen_id': fields.many2one('project.perilaku.absen.data.integration', 'Integrasi Data Absen Perilaku', readonly=True),
        'company_id': fields.many2one('res.company', 'OPD', readonly=True),
        'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai', readonly=True),
        
        'jumlah_masuk_kerja' :fields.integer('Jumlah Masuk Kerja',size=2, readonly=True),
        'jumlah_dinas_luar' :fields.integer('Jumlah Dinas Luar',size=2, readonly=True),
        'jumlah_diklat' :fields.integer('Jumlah Diklat',size=2, readonly=True),
        'jumlah_jam_kerja_normal' :fields.integer('Jumlah Jam Kerja Normal',size=3, readonly=True),
        'jumlah_jam_kerja_tambahan' :fields.integer('Jumlah Jam Kerja Tambahan',size=3, readonly=True),
        
        'jumlah_hari_kerja' :fields.integer('Jumlah Hari Kerja',size=2, readonly=True), #masuk Kerja + DL + DIK
        'jumlah_jam_kerja' :fields.integer('Jumlah Jam Kerja',size=3, readonly=True), # Jam Normal + Jam Tambahan
        'jumlah_hadir_apel_pagi' :fields.integer('Hadir Apel Pagi',size=1, readonly=True),
        'jumlah_hadir_upacara_hari_besar' :fields.integer('Hadir Upacara Hari Besar',size=1, readonly=True),
        
        'state': fields.selection([     ('new', 'Baru'), 
                                        ('synced', 'Synced'),
                                  ],
                                        'Status',
                                                     ),
        'notes': fields.text('Catatan' , readonly=True),
    }
    def sync_and_update_with_project_perilaku(self, cr, uid, ids,context=None):
        project_perilaku_pool = self.pool.get('project.perilaku')
        data_input_pool = self.pool.get('project.perilaku.absen.data.integration')
        now=time.strftime('%Y-%m-%d')
        extraction_ids = []
        integration_ids = []
        for data_extraction in self.browse(cr, uid, ids, context=context):
            task =  {}
            task.update({
                                    'old_realisasi_hadir_hari_kerja':data_extraction.project_perilaku_id.realisasi_hadir_hari_kerja,
                                    'old_realisasi_hadir_jam_kerja':data_extraction.project_perilaku_id.realisasi_hadir_jam_kerja,
                                    'old_realisasi_hadir_apel_pagi':data_extraction.project_perilaku_id.realisasi_hadir_apel_pagi,
                                    'old_realisasi_hadir_upacara_hari_besar':data_extraction.project_perilaku_id.realisasi_hadir_upacara_hari_besar,
                                    
                                    'realisasi_hadir_hari_kerja':data_extraction.jumlah_hari_kerja,
                                    'realisasi_hadir_jam_kerja':data_extraction.jumlah_jam_kerja,
                                    'realisasi_hadir_apel_pagi':data_extraction.jumlah_hadir_apel_pagi,
                                    'realisasi_hadir_upacara_hari_besar':data_extraction.jumlah_hadir_upacara_hari_besar,
                                    'absen_sync_date':now,
                                    'is_absen_sync':True,
                                    'absen_sync_state':'synced',
                                    'data_extraction_id':data_extraction.id
                                    
                        })
            project_perilaku_pool.write(cr,uid,data_extraction.project_perilaku_id.id,task,context=None)
            extraction_ids.append(data_extraction.id)
            integration_ids.append(data_extraction.project_skp_absen_id.id)
        if extraction_ids:
            self.write(cr, uid, extraction_ids, {'state':'synced'}, context=None)      
        if integration_ids:
            data_input_pool.write(cr, uid, integration_ids, {'state':'done'}, context=None)      
            
        return True
    def sync_and_update_with_project_skp_by_task_id(self, cr, uid, task_id,context=None):
        project_perilaku_pool = self.pool.get('project.perilaku')
        data_input_pool = self.pool.get('project.perilaku.absen.data.integration')
        now=time.strftime('%Y-%m-%d')
        extraction_ids = []
        integration_ids = []
        data_ids = self.search(cr, uid, [('project_perilaku_id', '=', task_id)], 
                                    context=None)
        for data_extraction in self.browse(cr, uid, data_ids, context=context):
            task =  {}
            task.update({
                                    'old_realisasi_hadir_hari_kerja':data_extraction.project_perilaku_id.realisasi_hadir_hari_kerja,
                                    'old_realisasi_hadir_jam_kerja':data_extraction.project_perilaku_id.realisasi_hadir_jam_kerja,
                                    'old_realisasi_hadir_apel_pagi':data_extraction.project_perilaku_id.realisasi_hadir_apel_pagi,
                                    'old_realisasi_hadir_upacara_hari_besar':data_extraction.project_perilaku_id.realisasi_hadir_upacara_hari_besar,
                                    
                                    'realisasi_hadir_hari_kerja':data_extraction.jumlah_hari_kerja,
                                    'realisasi_hadir_jam_kerja':data_extraction.jumlah_jam_kerja,
                                    'realisasi_hadir_apel_pagi':data_extraction.jumlah_hadir_apel_pagi,
                                    'realisasi_hadir_upacara_hari_besar':data_extraction.jumlah_hadir_upacara_hari_besar,
                                    'absen_sync_date':now,
                                    'is_absen_sync':True,
                                    'absen_sync_state':'synced',
                                    'data_extraction_id':data_extraction.id
                                    
                        })
            project_perilaku_pool.write(cr,uid,data_extraction.project_perilaku_id.id,task,context=None)
            extraction_ids.append(data_extraction.id)
            integration_ids.append(data_extraction.project_skp_absen_id.id)
        if extraction_ids:
            self.write(cr, uid, extraction_ids, {'state':'synced'}, context=None)      
        if integration_ids:
            data_input_pool.write(cr, uid, integration_ids, {'state':'done'}, context=None)      
            
        return True
    def rollback_sync_with_project_perilaku(self, cr, uid, ids,context=None):
        project_perilaku_pool = self.pool.get('project.perilaku')
        
        now=DateTime.today();
        extraction_ids = []
        integration_ids = []
        for data_extraction in self.browse(cr, uid, ids, context=context):
            task =  {}
            task.update({
                                    'old_realisasi_hadir_hari_kerja':0,
                                    'old_realisasi_hadir_jam_kerja':0,
                                    'old_realisasi_hadir_apel_pagi':0,
                                    'old_realisasi_hadir_upacara_hari_besar':0,
                                    'realisasi_hadir_hari_kerja':data_extraction.project_perilaku_id.old_realisasi_hadir_hari_kerja,
                                    'realisasi_hadir_jam_kerja':data_extraction.project_perilaku_id.realisasi_hadir_jam_kerja,
                                    'realisasi_hadir_apel_pagi':data_extraction.project_perilaku_id.realisasi_hadir_apel_pagi,
                                    'realisasi_hadir_upacara_hari_besar':data_extraction.project_perilaku_id.realisasi_hadir_upacara_hari_besar,
                                    'absen_sync_date':None,
                                    'is_absen_sync':False,
                                    'absen_sync_state':'not_synced',
                        })
            
            project_perilaku_pool.write(cr,uid,data_extraction.project_perilaku_id.id,task,context=None)
            extraction_ids.append(data_extraction.id)
        if extraction_ids:
            self.write(cr, uid, extraction_ids, {'state':'new'}, context=None)      
            
        return True
    def rollback_and_set_evaluated_sync_with_project_perilaku(self, cr, uid, ids,context=None):
        project_perilaku_pool = self.pool.get('project.perilaku')
        
       
        extraction_ids = []
        integration_ids = []
        project_perilaku_done_objs = []
        for data_extraction in self.browse(cr, uid, ids, context=context):
            task =  {}
            task.update({
                                    'old_realisasi_hadir_hari_kerja':0,
                                    'old_realisasi_hadir_jam_kerja':0,
                                    'old_realisasi_hadir_apel_pagi':0,
                                    'old_realisasi_hadir_upacara_hari_besar':0,
                                    'realisasi_hadir_hari_kerja':data_extraction.project_perilaku_id.old_realisasi_hadir_hari_kerja,
                                    'realisasi_hadir_jam_kerja':data_extraction.project_perilaku_id.realisasi_hadir_jam_kerja,
                                    'realisasi_hadir_apel_pagi':data_extraction.project_perilaku_id.realisasi_hadir_apel_pagi,
                                    'realisasi_hadir_upacara_hari_besar':data_extraction.project_perilaku_id.realisasi_hadir_upacara_hari_besar,
                                    'absen_sync_date':None,
                                    'is_absen_sync':False,
                                    'absen_sync_state':'not_synced',
                        })
            project_perilaku_pool.write(cr,uid,data_extraction.project_perilaku_id.id,task,context=None)
            extraction_ids.append(data_extraction.id)
            if data_extraction.project_perilaku_id.state == 'done' :
                project_perilaku_done_objs.append(data_extraction.project_perilaku_id)
        if extraction_ids:
            self.write(cr, uid, extraction_ids, {'state':'new'}, context=None)      
        #then evaluated this
        if project_perilaku_done_objs:
            for perilaku_obj in project_perilaku_done_objs:
                project_perilaku_pool.do_recalculate_poin(cr, perilaku_obj.user_id_bkd.id, [perilaku_obj.id], context)
        return True
project_perilaku_absen_data_extraction()
class project_perilaku_absen_extraction(osv.Model):
    _name = 'project.perilaku.absen.extraction'
    _description='Extract Data SKP - absen' 
    _columns = {
        'name': fields.char('Nama', size=100, ),
    }
project_perilaku_absen_extraction()

